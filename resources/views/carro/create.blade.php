<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Clientes</title>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<ul style="padding:10px;padding-left:25%;">
		<li><a href="/">Home</a></li>
	    <li><a href="cliente">Clientes</a></li>
	    <li><a href="carro">Veículos</a></li>
	    <li><a href="">Locações</a></li>
	    <li><a href="">Devoluções</a></li>
	</ul>
	<div style="margin-left:35%;">
		<h3>Cadastro de veículos</h3>
	</div>

	<a href="carrocreate" style="padding-left:38%;text-decoration:none;">Cadastrar clientes</a>
	<hr/>

 <form action="create_carro" method="post" style="padding-left:35%;">
	 <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <label>Modelo:</label><br><input type="text" name="modelo" id="modelo" required><br><br>
   <label>Cor:</label><br><input type="text" name="cor" id="cor" required><br><br>
   <label>Placa:</label><br> <input type="text" name="placa" id="placa" required><br><br>
  <label>Ano:</label><br> <input type="text" name="ano" id="ano" required><br><br>
  <label>Valor:</label><br> <input type="text" name="valor" id="valor" required><br><br>
  <label>Status:</label><br> <input type="text" name="status" id="status" required><br><br>
   <input type="submit" value="Cadastrar">

 </form>


 <footer class="footer">
 	<p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
 </footer>
</body>
</html>
