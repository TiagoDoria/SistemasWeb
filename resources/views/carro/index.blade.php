<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<title>Carro</title>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<ul style="padding:10px;padding-left:25%;">
		<li><a href="/">Home</a></li>
	    <li><a href="cliente">Clientes</a></li>
	    <li><a href="carro">Veículos</a></li>
	    <li><a href="">Locações</a></li>
	    <li><a href="">Devoluções</a></li>
	</ul>
	<div style="margin-left:35%;">
		<h3>Cadastro de Veículos</h3>
	</div>

	<a href="carrocreate" style="padding-left:38%;text-decoration:none;">Cadastrar Veículos</a>
	<hr/>


	<table border="1" style="margin-left:24%;">
		<tr >
			<th style="padding:10px;">ID</th>
			<th style="padding:10px;">Modelo</th>
			<th style="padding:10px;">Cor</th>
			<th style="padding:10px;">Placa</th>
			<th style="padding:10px;">Ano</th>
			<th style="padding:10px;">Valor</th>
			<th style="padding:10px;">Status</th>
			<th style="padding:10px;">Ações</th>
		</tr>
		@foreach ($carros as $carro)
			<tr>
				<td>{{$carro->id}}</td>
				<td>{{$carro->modelo}}</td>
				<td>{{$carro->cor}}</td>
				<td>{{$carro->placa}}</td>
				<td>{{$carro->ano}}</td>
				<td>{{$carro->valor}}</td>
				<td>{{$carro->status}}</td>
				<td>
					<a href="{{ url('/carro_edit', ['id'=>$carro->id]) }}">
						<button id="btnAdm" accesskey="a" >
							 <br>Editar</button>
					</a>
					 <a  onclick="javascript:if(!confirm('Deseja excluir??'))return false;" href="{{ url('/carro_destroy', ['id'=>$carro->id]) }}">
						 <button accesskey="a" >
						 <br>Deletar</button>
					</a>
				</td>
			</tr>
		@endforeach
	</table>
	<br>

	<footer class="footer">
		<p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
	</footer>


	<style type="text/css">
	html, body {
			background-color: #fff;
			color: #636b6f;
			font-family: 'Raleway', sans-serif;
			font-weight: 100;
			height: 100vh;
			margin: 0;
	}

	.full-height {
			height: 100vh;
	}

	.flex-center {
			align-items: center;
			display: flex;
			justify-content: center;
	}

	.position-ref {
			position: relative;
	}

	.content {
			text-align: center;
	}

	.title {
			font-size: 84px;
	}

	.m-b-md {
			margin-bottom: 30px;
	}

	#sectionmain{
		background-image: url('images/carro.jpg');
		background-size: 100%;
	}

	.footer {
		position:absolute;
		bottom:0;
		width:100%;
	}

	</style>
	<style type="text/css">
	ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
			background-color: #333;
	}

	li {
			float: left;
			border-right: 1px solid #bbb;
	}

	li a {
			display: block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
	}

	/* Change the link color to #111 (black) on hover */
	li a:hover {
			background-color: #111;
	}

	.active {
			background-color: #4CAF50;
	}

	li {
			border-right: 1px solid #bbb;
	}

	li:last-child {
			border-right: none;
	}
	</style>
</body>
</html>
