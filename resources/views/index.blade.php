<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <title>XPTZ Rent a Car</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>
      <section id="sectionmain">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                  Locadora XPTZ Rent a Car
                </div>
                <div>
                  <img src="images/car.png">
                </div>
                <br>
                <div class="links">
                    <a href="cliente">Clientes</a>
                    <a href="carro">Veículos</a>
                    <a href="">Locações</a>
                </div>
            </div>
        </div>
      </section>
      <footer class="footer">
        <p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
      </footer>

    </body>
</html>
