<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Editar clientes</title>

</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<ul style="padding:10px;padding-left:25%;">
		<li><a href="/">Home</a></li>
	    <li><a href="cliente">Clientes</a></li>
	    <li><a href="carro">Veículos</a></li>
	    <li><a href="">Locações</a></li>
	    <li><a href="">Devoluções</a></li>
	</ul>
	<div style="margin-left:35%;">
		<h3>Cadastro de Clientes</h3>
	</div>

	<a href="clientecreate" style="padding-left:38%;text-decoration:none;">Cadastrar clientes</a>
	<hr/>

 <form action="/edit_cliente" method="post" style="padding-left:35%;">
	 <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <input type="hidden" name="id" value="{{ $cliente->id }}">
   <label>Nome:</label><br><input type="text" name="nome" id="nome" value="{{$cliente->nome}}"><br><br>
   <label>CPF:</label><br><input type="text" name="cpf" id="cpf" value="{{$cliente->cpf}}"><br><br>
   <label>E-mail:</label><br> <input type="email" name="email" id="email" value="{{$cliente->email}}"><br><br>
  <label>Telefone:</label><br> <input type="text" name="telefone" id="telefone" value="{{$cliente->telefone}}"><br><br>
   <input type="submit" value="Editar">

 </form>


 <footer class="footer">
 	<p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
 </footer>

 <style type="text/css">
 html, body {
     background-color: #fff;
     color: #636b6f;
     font-family: 'Raleway', sans-serif;
     font-weight: 100;
     height: 100vh;
     margin: 0;
 }

 .full-height {
     height: 100vh;
 }

 .flex-center {
     align-items: center;
     display: flex;
     justify-content: center;
 }

 .position-ref {
     position: relative;
 }

 .content {
     text-align: center;
 }

 .title {
     font-size: 84px;
 }

 .m-b-md {
     margin-bottom: 30px;
 }

 #sectionmain{
   background-image: url('images/carro.jpg');
   background-size: 100%;
 }

 .footer {
   position:absolute;
   bottom:0;
   width:100%;
 }

 </style>
 <style type="text/css">
 ul {
     list-style-type: none;
     margin: 0;
     padding: 0;
     overflow: hidden;
     background-color: #333;
 }

 li {
     float: left;
     border-right: 1px solid #bbb;
 }

 li a {
     display: block;
     color: white;
     text-align: center;
     padding: 14px 16px;
     text-decoration: none;
 }

 /* Change the link color to #111 (black) on hover */
 li a:hover {
     background-color: #111;
 }

 .active {
     background-color: #4CAF50;
 }

 li {
     border-right: 1px solid #bbb;
 }

 li:last-child {
     border-right: none;
 }
 </style>
</body>
</html>
