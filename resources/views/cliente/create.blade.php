<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Clientes</title>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<ul style="padding:10px;padding-left:25%;">
		<li><a href="/">Home</a></li>
	    <li><a href="cliente">Clientes</a></li>
	    <li><a href="carro">Veículos</a></li>
	    <li><a href="">Locações</a></li>
	    <li><a href="">Devoluções</a></li>
	</ul>
	<div style="margin-left:35%;">
		<h3>Cadastro de Clientes</h3>
	</div>

	<a href="clientecreate" style="padding-left:38%;text-decoration:none;">Cadastrar clientes</a>
	<hr/>

 <form action="create_cliente" method="post" style="padding-left:35%;">
	 <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <label>Nome:</label><br><input type="text" name="nome" id="nome" required><br><br>
   <label>CPF:</label><br><input type="text" name="cpf" id="cpf" required><br><br>
   <label>E-mail:</label><br> <input type="email" name="email" id="email" required><br><br>
  <label>Telefone:</label><br> <input type="text" name="telefone" id="telefone" required><br><br>
   <input type="submit" value="Cadastrar">

 </form>


 <footer class="footer">
 	<p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
 </footer>
</body>
</html>
