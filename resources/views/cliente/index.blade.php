<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<title>Clientes</title>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<ul style="padding:10px;padding-left:25%;">
		<li><a href="/">Home</a></li>
	    <li><a href="cliente">Clientes</a></li>
	    <li><a href="carro">Veículos</a></li>
	    <li><a href="">Locações</a></li>
	    <li><a href="">Devoluções</a></li>
	</ul>
	<div style="margin-left:35%;">
		<h3>Cadastro de Clientes</h3>
	</div>

	<a href="clientecreate" style="padding-left:38%;text-decoration:none;">Cadastrar clientes</a>
	<hr/>


	<table border="1" style="margin-left:24%;">
		<tr >
			<th style="padding:10px;">ID</th>
			<th style="padding:10px;">Nome</th>
			<th style="padding:10px;">CPF</th>
			<th style="padding:10px;">E-mail</th>
			<th style="padding:10px;">Telefone</th>
			<th style="padding:10px;">Ações</th>
		</tr>
		@foreach ($clientes as $cliente)
			<tr>
				<td>{{$cliente->id}}</td>
				<td>{{$cliente->nome}}</td>
				<td>{{$cliente->cpf}}</td>
				<td>{{$cliente->email}}</td>
				<td>{{$cliente->telefone}}</td>
				<td>
					<a href="{{ url('/cliente_edit', ['id'=>$cliente->id]) }}">
						<button id="btnAdm" accesskey="a" >
							 <br>Editar</button>
					</a>
					 <a  onclick="javascript:if(!confirm('Deseja excluir??'))return false;" href="{{ url('/cliente_destroy', ['id'=>$cliente->id]) }}">
						 <button accesskey="a" >
						 <br>Deletar</button>
					</a>
				</td>
			</tr>
		@endforeach
	</table>

	<footer class="footer">
		<p style="margin-left:23%; color:black;"> Desenvolvido por: Tiago Dória, Florencia Malenchini, Mauricio Marques,  Leonora, Alan Atta</p>
	</footer>


	<style type="text/css">
	html, body {
			background-color: #fff;
			color: #636b6f;
			font-family: 'Raleway', sans-serif;
			font-weight: 100;
			height: 100vh;
			margin: 0;
	}

	.full-height {
			height: 100vh;
	}

	.flex-center {
			align-items: center;
			display: flex;
			justify-content: center;
	}

	.position-ref {
			position: relative;
	}

	.content {
			text-align: center;
	}

	.title {
			font-size: 84px;
	}

	.m-b-md {
			margin-bottom: 30px;
	}

	#sectionmain{
		background-image: url('images/carro.jpg');
		background-size: 100%;
	}

	.footer {
		position:absolute;
		bottom:0;
		width:100%;
	}

	</style>
	<style type="text/css">
	ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
			background-color: #333;
	}

	li {
			float: left;
			border-right: 1px solid #bbb;
	}

	li a {
			display: block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
	}

	/* Change the link color to #111 (black) on hover */
	li a:hover {
			background-color: #111;
	}

	.active {
			background-color: #4CAF50;
	}

	li {
			border-right: 1px solid #bbb;
	}

	li:last-child {
			border-right: none;
	}
	</style>
</body>
</html>
