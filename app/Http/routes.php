<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('/', 'LocadoraController@index');
Route::post('/', 'LocadoraController@index');
Route::get('cliente', 'ClienteController@index');
Route::get('clientecreate', 'ClienteController@create');
Route::get('carro', 'CarroController@index');
Route::get('home', 'LocadoraController@index');
Route::post('create_cliente', 'ClienteController@create_cliente');
Route::post('create_edit', 'ClienteController@edit');
Route::get('cliente_edit/{id}','ClienteController@cliente_edit');
Route::post('edit_cliente','ClienteController@edit');
Route::get('cliente_destroy/{id}','ClienteController@destroy');
