<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Cliente;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
  public function index(){
    $clientes = DB::table('cliente')->get();

    return view('cliente.index', ['clientes' => $clientes]);
  }

  public function create(){
    return View('cliente.create');
  }

  public function create_cliente(Request $request){
    $nome = $request->input('nome');
    $cpf = $request->input('cpf');
    $email = $request->input('email');
    $telefone = $request->input('telefone');

    DB::insert('insert into cliente (nome,cpf,email,telefone) values (?,?,?,?)',
    array($nome,$cpf,$email,$telefone));
    return View('cliente.create');
  }

  public function cliente_edit($id)   {
    $cliente = DB::table('cliente')->where('id',$id)->first();
    return view( 'cliente.edit',compact('cliente'));
  }

  public function edit(Request $request){
    $data = [
      'nome'=>$request->nome,
      'cpf'=>$request->cpf,
      'email'=>$request->email,
      'telefone'=>$request->telefone,
      'id'=>$request->id];
      DB::table('cliente')->where('id',$request->id)->update($data);
      $clientes = DB::table('cliente')->get();
      return view('cliente.index', ['clientes' => $clientes]);

    }

     public function destroy($id)   {
           cliente::find($id)->delete();
           $clientes = DB::table('cliente')->get();
           return view('cliente.index', ['clientes' => $clientes]);
      }
  }
