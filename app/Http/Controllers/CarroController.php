<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Carro;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CarroController extends Controller
{
  public function index(){
    $carros = DB::table('carro')->get();

    return view('carro.index', ['carros' => $carros]);
  }

  public function create(){
    return View('carro.create');
  }

  public function create_carro(Request $request){
    $modelo = $request->input('modelo');
    $cor = $request->input('cor');
    $placa = $request->input('placa');
    $ano = $request->input('ano');
    $valor = $request->input('valor');
    $status = $request->input('status');

    DB::insert('insert into carro (modelo,cor,placa,ano,valor,status) values (?,?,?,?,?,?)',
    array($modelo,$cor,$placa,$ano,$valor,$status));
    return View('carro.create');
  }

  public function carro_edit($id)   {
    $carro = DB::table('carro')->where('id',$id)->first();
    return view( 'carro.edit',compact('carro'));
  }

  public function edit(Request $request){
    $data = [
      'modelo'=>$request->modelo,
      'cor'=>$request->cor,
      'placa'=>$request->placa,
      'ano'=>$request->ano,
      'valor'=>$request->valor,
      'status'=>$request->status,
      'id'=>$request->id];
      DB::table('carro')->where('id',$request->id)->update($data);
      $carros = DB::table('carro')->get();
      return view('carro.index', ['carros' => $carros]);

    }

     public function destroy($id)  {
           carro::find($id)->delete();
           $carros = DB::table('carro')->get();
           return view('carro.index', ['carros' => $carros]);
      }
  }
