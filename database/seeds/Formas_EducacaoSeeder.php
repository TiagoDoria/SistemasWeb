<?php

use Illuminate\Database\Seeder;

class Formas_EducacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $formas = [
                0 =>[ 'descricao' => 'formal'],
                1 =>[ 'descricao' => 'naoformal'],
                2 =>[ 'descricao' => 'informal']
            ];
        DB::table('formaeducacao')->insert($formas);
    }
}
