<?php

use Illuminate\Database\Seeder;

class Nivel_EducacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $nivel = [
              0 =>[ 'descricao' => 'infantil'],
              1 =>[ 'descricao' => 'fundamental'],
              2 =>[ 'descricao' => 'medio'],
              3 =>[ 'descricao' => 'profissional'],
              4 =>[ 'descricao' => 'superior']
          ];
      DB::table('niveleducacao')->insert($nivel);
    }
}
