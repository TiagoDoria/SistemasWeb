<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modelo');
            $table->string('cor');
            $table->string('placa');
            $table->string('valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carro', function (Blueprint $table) {
            //
        });
    }
}
